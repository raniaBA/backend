<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

    Route::get('/home', ['as' => 'index', 'uses' => 'IndexController@showIndex']);
    Route::get('/contact', ['as' => 'contact', 'uses' => 'IndexController@contact']);
    Route::get('/messages/{email}', ['as' => 'messages', 'uses' => 'ContactController@messages']);

    Route::get('/adminMap', 'ZoneController@show');
    Route::get('/clientMap', 'ZoneController@showClient');
    Route::get('/', ['as' => 'index', 'uses' => 'IndexController@showIndex']);
    Route::post('send', ['as' => 'send', 'uses' => 'ContactController@send']);
//
Route::get('/deleteMsg/{id}',['as'=>'deleteMsg','uses'=>'ContactController@delete']);

    Route::get('/categ', ['as' => 'listing', 'uses' => 'CategorieController@showCategorie']);

    //Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/home', ['as' => 'index', 'uses' => 'IndexController@showIndex']);

    Route::get('/map', 'map@show');
    Route::get('/profile', 'profileController@showProfile');

    Route::get('/', ['as' => 'index', 'uses' => 'IndexController@showIndex']);


    Route::get('/categ', ['as' => 'listing', 'uses' => 'CategorieController@showCategorie']);

//Route::get('/editCateg',['as'=>'listing','uses'=>'CategorieController@editCategorie']);
//Route::get('/addCateg',['as'=>'listing','uses'=>'CategorieController@addCategorie']);

    Route::get('/listProd', ['as' => 'listing', 'uses' => 'ProduitController@showProduit']);


    Route::get('/editProd/{id}', ['as' => 'listing', 'uses' => 'ProduitController@edit']);
    Route::post('/editProd/{id}', 'ProduitController@update');
    Route::post('/modif/{id}', 'OrdersController@modif');

    Route::get('/addProd', ['as' => 'listing', 'uses' => 'ProduitController@addProduit']);
    Route::post('/addProd', 'ProduitController@store');

//Route::get('/delete/{id}',['as'=>'listing','uses'=>'ProduitController@destroy']);
//Route::ressource('produit','ProduitController');

    Route::delete('/delete/{id}', 'ProduitController@destroy');
    Route::get('/order', 'OrdersController@show');


    Route::get('/editProd', ['as' => 'listing', 'uses' => 'ProduitController@editProduit']);
    Route::get('/addProd', ['as' => 'listing', 'uses' => 'ProduitController@addProduit']);
    Route::resource('categorie', 'CategorieController');
    Route::resource('zone', 'ZoneController');
    Route::resource('payment', 'PaymentMethodController');

    Route::delete('/delete/{id}', 'ProduitController@destroy');
    Route::get('/recette', 'RecetteController@show');
    Route::post('/filter', 'RecetteController@filter');
    Route::resource('categorie', 'CategorieController');
    Route::resource('orders', 'OrdersController');
    Route::resource('users', 'UsersController');
//Route::get('/find/{id}','UsersController@find');

    Route::get('/find/{id}', ['as' => 'find', 'uses' => 'UsersController@find']);
    Route::post('/profileSetting/{id}', ['as' => 'profileSetting', 'uses' => 'profileController@profileSetting']);
    Route::post('/changePassword/{id}', ['as' => 'changePassword', 'uses' => 'profileController@changePassword']);
//Route::resource('recette', 'RecetteController');
    Route::get('/showlivre', 'OrdersController@showLivre');






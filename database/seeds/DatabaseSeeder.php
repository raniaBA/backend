<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => Str::random(10),
            'parent' => "1",
            'icon' => Str::random(10),
            'created_at' => "2019-06-26 02:00:00",
            'updated_at' => "2019-06-26 02:00:00",
        ]);
        DB::table('produits')->insert([
            'name' => Str::random(10),
            'description' => Str::random(10),
            'image' => Str::random(10),
            'price' => "1.5",
            'categorie_id' => "1",
            'created_at' => "2019-06-26 02:00:00",
            'updated_at' => "2019-06-26 02:00:00",
        ]);
    }
}

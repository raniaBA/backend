<html>
<head>
    <title> La caléche - @yield("title")</title>
@section("topCss")
    <!-- Favicon-->
        <link href="{{ URL::asset("img/apple-touch-icon.png")}}"  rel="apple-touch-icon" >
        <!-- Google Fonts -->
        <link href="{{ URL::asset("https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext")}}" rel="stylesheet" type="text/css">
        <link href="{{ URL::asset("https://fonts.googleapis.com/icon?family=Material+Icons")}}" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="{{ URL::asset("plugins/bootstrap/css/bootstrap.css")}}" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="{{ URL::asset("plugins/node-waves/waves.css")}}" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="{{ URL::asset("plugins/animate-css/animate.css")}}" rel="stylesheet" />

        <!-- Morris Chart Css-->
        <link href="{{ URL::asset("plugins/morrisjs/morris.css")}}" rel="stylesheet" />

        <!-- Custom Css -->
        <link href="{{ URL::asset("css/style.css")}}" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="{{ URL::asset("css/themes/all-themes.css")}}" rel="stylesheet" />
@show
@section("topJs")
    <!-- Scripts -->
    @show

</head>
<body class="theme-red @yield('bodyStyle')">


@section("header")

    @include("partiels.header")

@show
@section("menu")

    @include("partiels.menu")

@show


@yield("content")

{{--  <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
      <div class="container">
          <a class="navbar-brand" href="{{ url('/') }}">
              {{ config('app.name', 'Laravel') }}
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
              <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <!-- Left Side Of Navbar -->
              <ul class="navbar-nav mr-auto">

              </ul>

              <!-- Right Side Of Navbar -->
              <ul class="navbar-nav ml-auto">
                  <!-- Authentication Links -->
                  @guest
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                      </li>
                      @if (Route::has('register'))
                          <li class="nav-item">
                              <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                          </li>
                      @endif
                  @else
                      <li class="nav-item dropdown">
                          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                              {{ Auth::user()->name }} <span class="caret"></span>
                          </a>

                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                  {{ __('Logout') }}
                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
                          </div>
                      </li>
                  @endguest
              </ul>
          </div>
      </div>
  </nav>--}}



@section("footerJs")

    <!-- Jquery Core Js -->
    <script src="{{ URL::asset("plugins/jquery/jquery.min.js")}}" ></script>
    <!-- Bootstrap Core Js -->
    <script src="{{ URL::asset("plugins/bootstrap/js/bootstrap.js")}}" ></script>
    <!-- Select Plugin Js -->
    <script src="{{ URL::asset("plugins/bootstrap-select/js/bootstrap-select.js")}}" ></script>
    <!-- Slimscroll Plugin Js -->
    <script src="{{ URL::asset("plugins/jquery-slimscroll/jquery.slimscroll.js")}}" ></script>
    <!-- Waves Effect Plugin Js -->
    <script src="{{ URL::asset("plugins/node-waves/waves.js")}}" ></script>
    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ URL::asset("plugins/jquery-countto/jquery.countTo.js")}}" ></script>
    <!-- Morris Plugin Js -->
    <script src="{{ URL::asset("plugins/raphael/raphael.min.js")}}" ></script>
    <script src="{{ URL::asset("plugins/morrisjs/morris.js")}}" ></script>
    <!-- ChartJs -->
    <script src="{{ URL::asset("plugins/chartjs/Chart.bundle.js")}}" ></script>
    <!-- Flot Charts Plugin Js -->
    <script src="{{ URL::asset("plugins/flot-charts/jquery.flot.js")}}" ></script>
    <script src="{{ URL::asset("plugins/flot-charts/jquery.flot.resize.js")}}" ></script>
    <script src="{{ URL::asset("plugins/flot-charts/jquery.flot.pie.js")}}" ></script>
    <script src="{{ URL::asset("plugins/flot-charts/jquery.flot.categories.js")}}" ></script>
    <script src="{{ URL::asset("plugins/flot-charts/jquery.flot.time.js")}}" ></script>
    <!-- Sparkline Chart Plugin Js -->
    <script src="{{ URL::asset("plugins/jquery-sparkline/jquery.sparkline.js")}}" ></script>
    <!-- Custom Js -->
    <script src="{{ URL::asset("js/admin.js")}}" ></script>
    <!-- Demo Js -->
    <script src="{{ URL::asset("js/demo.js")}}" ></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSkRhBbrsIG6XBn8Xt5qGBny6Anh6VXk8&libraries=drawing&callback=initMap"
            async defer></script>
@show
</body>
</html>

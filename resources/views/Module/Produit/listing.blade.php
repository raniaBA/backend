@extends('layouts/app')
@section('title','produit')
@section('topCss')
    @parent
     <link href="{{ URL::asset("plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css")}}" rel="stylesheet">

@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
             <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                PRODUCT TABLE
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @endif
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Image</th>
                                        <th>Price</th>
                                        <th>categorie</th>
                                        <th>Created_at</th>
                                        <th>Updated_at</th>
                                        <th>Option</th>
                                        <th>Option</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Image</th>
                                        <th>Price</th>
                                        <th>categorie</th>
                                        <th>Created_at</th>
                                        <th>Updated_at</th>
                                        <th>Option</th>
                                        <th>Option</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>

                                    @foreach($produits  as $produit)

                                        <tr>
                                            <td tabindex="1">{{$produit->name}}</td>
                                            <td tabindex="1">{{$produit->description}}</td>
                                            <td tabindex="1"><img src="{{$produit->image}}"></td>
                                            <td tabindex="1">{{$produit->price}}</td>
                                            <td tabindex="1">{{$produit->categorie_id}}</td>
                                            <td tabindex="1">{{$produit->created_at}}</td>
                                            <td tabindex="1">{{$produit->updated_at}}</td>
                                            <td tabindex="1"><a href="{{action('ProduitController@edit',$produit->id)}}" type="submit" class="btn btn-primary waves-effect">Modifier</a></td>

                                            <td tabindex="1">
                                                <form  action="{{action('ProduitController@destroy',$produit->id)}}" method="post">
                                                    {{csrf_field()}}
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button type="submit" class="btn btn-primary waves-effect">Supprimer</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
@section("footerJs")
    @parent
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ URL::asset("plugins/jquery-datatable/jquery.dataTables.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/buttons.flash.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/jszip.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/pdfmake.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/vfs_fonts.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/buttons.html5.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/buttons.print.min.js")}}"></script>

    <script src="{{ URL::asset("js/pages/tables/jquery-datatable.js")}}"></script>
@endsection
@extends('layouts/app')
@section('title','produit')
@section('topCss')
    @parent

    <link href="{{ URL::asset("../../plugins/bootstrap-select/css/bootstrap-select.css")}}" rel="stylesheet"/>
@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>AJOUT PRODUIT</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another
                                            action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else
                                            here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div><br/>
                        @endif
                        <form id="form_advanced_validation" method="POST" novalidate="novalidate"
                              action="{{url('/addProd')}}" enctype="multipart/form-data">
                            <input type="hidden" value="{{csrf_token()}}" name="_token"/>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="name" required=""
                                           aria-required="true">
                                    <label class="form-label">Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="description" required=""
                                           aria-required="true">
                                    <label class="form-label">Description</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="file" name="image" accept="image/*" id="image">
                                    {{--<label class="form-label">Image</label>--}}
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" class="form-control" name="price" required=""
                                           aria-required="true">
                                    <label class="form-label">Price</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <p>
                                        <b>Catégorie</b>
                                    </p>
                                    <select name="categorie_id" class="form-control show-tick" data-show-subtext="true">
                                        @foreach($categories  as $categorie))
                                        <option>{{$categorie->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>
@endsection
@section("footerJs")
    @parent
@endsection
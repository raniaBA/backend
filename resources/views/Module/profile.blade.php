@extends('layouts/app')
@section('title','profile')
@section('topCss')
    @parent

@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12 col-sm-3">
                <div class="card profile-card">
                    <div class="profile-header">&nbsp;</div>
                    <div class="profile-body">
                        <div class="image-area">
                            <img src="../../images/user-lg.jpg" alt="AdminBSB - Profile Image" />
                        </div>
                        <div class="content-area">
                            <h3>{{ Auth::user()->name }}</h3>
                            <p>Web Software Developer</p>
                            <p>Administrator</p>
                        </div>
                    </div>

                </div>


            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="card">
                    <div class="body">
                        <div>
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div><br/>
                            @endif
                                @if(session()->get('success'))
                                    <div class="alert alert-success">
                                        {{ session()->get('success') }}
                                    </div><br />
                                @endif
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#profile_settings" aria-controls="settings" role="tab" data-toggle="tab">Profile Settings</a></li>
                                <li role="presentation"><a href="#change_password_settings" aria-controls="settings" role="tab" data-toggle="tab">Change Password</a></li>
                            </ul>

                            <div class="tab-content">



                                <div role="tabpanel" class="tab-pane fade in active" id="profile_settings">
                                    <form id="form_advanced_validation" novalidate="novalidate" class="form-horizontal"action="{{action('profileController@profileSetting',Auth::user()->id )}}" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <label for="NameSurname" class="col-sm-2 control-label">Name Surname</label>
                                            <div class="col-sm-10">
                                                <div class="form-line">
                                                    <input type="text" class="form-control"   name="name" placeholder="Name Surname" value="{{ Auth::user()->name }}" required="" aria-required="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Email" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-10">
                                                <div class="form-line">
                                                    <input type="email" class="form-control"  name="email" placeholder="Email" value="{{ Auth::user()->email }}" required="" aria-required="true">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-danger">SUBMIT</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>


                                <div role="tabpanel" class="tab-pane fade in" id="change_password_settings">

                                    <form class="form-horizontal"action="{{action('profileController@changePassword',Auth::user()->id )}}" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Old Password</label>
                                            <div class="col-sm-9">
                                                <div class="form-line">
                                                    <input type="password" class="form-control"   name="old" placeholder="Old Password" required="" aria-required="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="NewPassword" class="col-sm-3 control-label">New Password</label>
                                            <div class="col-sm-9">
                                                <div class="form-line">
                                                    <input type="password" class="form-control" name="new" placeholder="New Password" required="" aria-required="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="NewPasswordConfirm" class="col-sm-3 control-label">New Password (Confirm)</label>
                                            <div class="col-sm-9">
                                                <div class="form-line">
                                                    <input type="password" class="form-control"  name="new2" placeholder="New Password (Confirm)" required="" aria-required="true">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <button type="submit" class="btn btn-danger">SUBMIT</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section("footerJs")
    @parent
@endsection
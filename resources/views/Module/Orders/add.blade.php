@extends('layouts/app')
@section('title','order')
@section('topCss')
    @parent

    <link href="{{ URL::asset("../../plugins/bootstrap-select/css/bootstrap-select.css")}}" rel="stylesheet"/>
@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>AJOUT COMMANDE</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another
                                            action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else
                                            here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div><br/>
                        @endif
                        <form id="form_advanced_validation" method="POST" novalidate="novalidate"
                              action="{{route('orders.store')}}">
                            <input type="hidden" value="{{csrf_token()}}" name="_token"/>
                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <p>
                                        <b>nombre de produit</b>
                                    </p>
                                    <select id="test-dropdown" name="nbr" onchange="choice1(this)"
                                            class="form-control show-tick"
                                            data-show-subtext="true">
                                        <option></option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>

                                    </select>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <p>
                                        <b>nom produit</b>
                                    </p>
                                    <select name="produit_id1" class="form-control show-tick"
                                            data-show-subtext="true">
                                        @foreach($produit  as $p))
                                        <option>{{$p->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Product quantity</label>
                                    <input type="number" class="form-control" name="product_quantity1" required=""
                                           aria-required="true">
                                </div>
                            </div>
                            <div id="ajout"></div>


                            <div class="form-group form-float">
                                <b>Date Time</b>
                                <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                    <div class="form-line">
                                        <input type="text" name="delivery_date_time" class="form-control datetime"
                                               placeholder="Ex: 30/07/2016 23:59">
                                    </div>
                                </div>
                            </div>


                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>
@endsection
@section("footerJs")
    @parent
    <script>

        function choice1(select) {
            var val = select.options[select.selectedIndex].text;
            var div = '';

            for (j = 2; j <= val; j++) {

                div = '<div class="row clearfix">' +
                    '<div class="col-md-3">' +
                    '<p><b>nom produit</b></p>' +
                    '<select name="produit_name' + j + '" class="form-control show-tick" data-show-subtext="true">' +
                    '@foreach($produit  as $p))' +
                    '<option>{{$p->name}}</option>' +
                    '@endforeach' +
                    '</select>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group form-float">' +
                    '<div class="form-line">' +
                    '<label class="form-label">Product quantity</label>' +
                    '<input type="number" class="form-control" name="product_quantity' + j + '" required="" aria-required="true">' +
                    '</div>' +
                    '</div>';
                document.getElementById("ajout").innerHTML += div;
            }
        }
    </script>
@endsection
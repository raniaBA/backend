@extends('layouts/app')
@section('title','orders')
@section('topCss')
    @parent

    <link href="{{ URL::asset("../../plugins/bootstrap-select/css/bootstrap-select.css")}}" rel="stylesheet"/>
@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>MODIFICATION COMMMANDE</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another
                                            action</a></li>
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else
                                            here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div><br/>
                        @endif
                        <form method="post" action="{{ route('orders.update', $orders->id) }}">
                            @method('PATCH')
                            @csrf
                            @for($i=0; $i<count($produit);$i++)
                                {{--<div class="form-group form-float">--}}
                                    {{--<div class="form-line">--}}
                                        {{--<input type="text" class="form-control" name="name{{$i}}"--}}
                                               {{--value="{{$produit[$i]->name}}" >--}}
                                        {{--<label class="form-label">Product name</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="row clearfix">
                                    <div class="col-md-3">
                                        <p>
                                            <b>nom produit</b>
                                        </p>
                                        <select name="produit_name{{$i}}"  class="form-control show-tick"
                                                data-show-subtext="true">
                                            @foreach($p  as $prod))
                                            <option>{{$prod->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" class="form-control" name="product_quantity{{$i}}"
                                           value="{{$pivot[$i]->quantity}}" >
                                    <label class="form-label">Product quantity</label>
                                </div>
                            </div>
                                {{--<div class="form-group form-float">--}}
                                    {{--<div class="form-line">--}}
                                        {{--<input type="number" class="form-control" name="price{{$i}}"--}}
                                               {{--value="{{$produit[$i]->price}}" >--}}
                                        {{--<label class="form-label">Price</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <hr style="color: red">
                            @endfor
                            {{--<div class="form-group form-float">--}}
                                {{--<div class="form-line">--}}
                                    {{--<input type="number" class="form-control" name="total_price"--}}
                                           {{--value="{{$orders->total_price}}">--}}
                                    {{--<label class="form-label">Total price</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group form-float">
                                <b>Date Time</b>
                                <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                    <div class="form-line">
                                        <input type="text" name="delivery_date_time" class="form-control datetime"
                                               value="{{$orders->delivery_date_time}}"
                                               placeholder="Ex: 30/07/2016 23:59">
                                    </div>
                                </div>
                            </div>

                            <div class="input-group input-group-lg">
                                <input type="radio" id="contactChoice1"class="with-gap"
                                         name="status" value="1" checked>
                                <label for="contactChoice1">En attente</label>

                                <input type="radio" id="contactChoice2" class="with-gap"
                                       name="status" value="2">
                                <label for="contactChoice2">Livré</label>
                                <input type="radio" id="contactChoice3" class="with-gap"
                                       name="status" value="3">
                                <label for="contactChoice3">en cours</label>
                            </div>


                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>
@endsection
@section("footerJs")
    @parent
@endsection
@extends('layouts.app')
@section('title','map')
@section('topCss')
    @parent

@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <!-- Basic Example -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                CLIENT MAP
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div id="map" class="gmap"></div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Example -->
            @section("footerJs")
                @parent
                <script>


                    function initMap() {

                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 17,
                            center: {lat: 36.818338, lng: 10.178565},
                            mapTypeId: 'terrain'
                        });


                        var Coords = [
                                @for($i=0;$i<count($poly); $i=$i+2)
                            {
                                lat: parseFloat(['{{$poly[$i]}}']), lng: parseFloat(['{{$poly[$i+1]}}'])
                            },
                            @endfor
                        ];
                        console.log('cord', Coords);


                        // var Coords = [
                        // {lat:36.81908990932413 , lng: 10.177321646491464},
                        // {lat:36.81916720824955 , lng: 10.179703448096689},
                        // {lat:36.81845433742188 , lng: 10.176871035376962}
                        // ];

                        // Construct the polygon.
                        var zone = new google.maps.Polygon({
                            paths: Coords,
                            strokeColor: '#FF0000',
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            fillColor: '#FF0000',
                            fillOpacity: 0.35
                        });

                        // Marker.
                        var marker = new google.maps.Marker({
                            position: {lat: 36.818338, lng: 10.178565},
                            animation: google.maps.Animation.DROP,
                            map: map,
                            label: {text: "La caléche!", color: "white"}
                        });
                        // Marker.
                        var lastPosition = {lat: 36.818166, lng: 10.179091};
                        var marker2 = new google.maps.Marker({
                            position: {lat: 36.818166, lng: 10.179091},

                            animation: google.maps.Animation.DROP,
                            draggable: true,
                            map: map,
                            label: {text: "My place!", color: "white"}
                        });
                        var infowindow = new google.maps.InfoWindow({
                            content: "<strong>La Calèche</strong>, Avenue de La Liberté, Jeanne d'Arc"
                        });
                        marker.addListener('click', function () {
                            infowindow.open(map, marker);
                        });
                        google.maps.event.addListener(marker2, 'dragend', function (e) {

                            if (google.maps.geometry.poly.containsLocation(e.latLng, zone)) {
                                alert(marker2.getPosition());

                            }
                            else {
                                alert("Out of zone");
                                marker2.setPosition(lastPosition);
                            }

                        });
                        zone.setMap(map);

                    }

                </script>


        </div>
    </section>
@endsection
{{--@section("footerJs")--}}
{{--@parent--}}


{{--@endsection--}}
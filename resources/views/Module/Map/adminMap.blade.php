@extends('layouts.app')
@section('title','map')
@section('topCss')
    @parent

@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ADMIN MAP
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div id="gmap_basic_example" class="gmap"></div>
                            <div id="info"></div>
                            <form id="form" method="post" action="{{route('zone.store')}}">
                                @csrf
                                <button type="submit" id="myField" name="polygone"
                                        class="btn bg-light-green waves-effect">
                                    <i class="material-icons">save</i>
                                    <span>SAVE COORDS</span>
                                </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @section("footerJs")
                @parent

                <script>

                    var geocoder;

                    var polygonArray = [];

                    function initMap() {
                        var map = new google.maps.Map(document.getElementById('gmap_basic_example'), {
                            center: {lat: 36.818338, lng: 10.178565},
                            zoom: 17
                        });
                        // Marker.
                        var marker = new google.maps.Marker({
                            position: {lat: 36.818338, lng: 10.178565},
                            animation: google.maps.Animation.DROP,
                            map: map,
                            label: {text: "La caléche!", color: "white"}
                        });
                        var drawingManager = new google.maps.drawing.DrawingManager({
                            drawingMode: google.maps.drawing.OverlayType.MARKER,
                            drawingControl: true,
                            drawingControlOptions: {
                                position: google.maps.ControlPosition.TOP_CENTER,
                                drawingModes: ['polygon']
                            },
                            markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
                            circleOptions: {
                                fillColor: '#ffff00',
                                fillOpacity: 1,
                                strokeWeight: 5,
                                clickable: false,
                                editable: true,
                                zIndex: 1
                            }
                        });

                        drawingManager.setMap(map);

                        $('#form').hide();
                        google.maps.event.addListener(drawingManager, 'polygoncomplete', function (e) {
                            document.getElementById('info').innerHTML += "polygon points:" + "<br>";
                            for (var i = 0; i < e.getPath().getLength(); i++) {
                                document.getElementById('info').innerHTML += e.getPath().getAt(i).toUrlValue(6) + "<br>";
                            }
                            var coordinates = (e.getPath().getArray()) + '';
                            {{--document.getElementById('info').innerHTML+="<a href=\"{{ url('/zone')}}\"  type=\"submit\" class=\"btn bg-light-green waves-effect\"><i class=\"material-icons\">chat</i><span>CHAT</span></a>";--}}
                            $('#form').show();
                            document.getElementById('myField').setAttribute('value', coordinates);

                            console.log(coordinates);
                        });


                    }
                </script>


        </div>
    </section>
@endsection
@section("footerJs")
    @parent


@endsection
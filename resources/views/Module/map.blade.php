@extends('layouts/app')
@section('title','map')
@section('topCss')
    @parent

@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>

        @section("clientMap")

            @include("partiels.clientMap")

        @show
            @section("adminMap")

                @include("partiels.adminMap")

            @show


        </div>
    </section>
@endsection
@section("footerJs")
    @parent


@endsection
@extends('layouts/app')
@section('title','recette')
@section('topCss')
    @parent
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ URL::asset("plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css")}}" rel="stylesheet" />
    <!-- Bootstrap DatePicker Css -->
    <link href="{{ URL::asset("plugins/bootstrap-datepicker/css/bootstrap-datepicker.css")}}" rel="stylesheet" />
    <link href="{{ URL::asset("plugins/bootstrap-select/css/bootstrap-select.css")}}" rel="stylesheet" />



    <link href="{{ URL::asset("https://fonts.googleapis.com/icon?family=Material+Icons")}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset("https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext")}}" rel="stylesheet" type="text/css">
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ URL::asset("plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css")}}" rel="stylesheet" />

    <!-- Bootstrap DatePicker Css -->
    <link  href="{{ URL::asset("plugins/bootstrap-datepicker/css/bootstrap-datepicker.css")}}" rel="stylesheet" />

    <!-- Wait Me Css -->
    <link href="{{ URL::asset("plugins/waitme/waitMe.css")}}" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="{{ URL::asset("plugins/bootstrap-select/css/bootstrap-select.css")}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ URL::asset("css/style.css")}}" rel="stylesheet">

    {{--<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->--}}
    {{--<link href="{{ URL::asset("css/themes/all-themes.css")}}" rel="stylesheet" />--}}



@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>


                 <div class="card">
                    <div class="header">
                        <h2>
                            COLORFUL PANEL ITEMS WITH ICON
                        </h2>
                         <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">

                         <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="body">
                                        <form method="post" action="{{ URL::asset('/filter') }}">
                                            @csrf
                                        <div class="row clearfix">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" name="debut" class="datepicker form-control" placeholder="Please choose a date start...">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" name="fin" class="datepicker form-control" placeholder="Please choose a date end...">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <button type="submit" class="btn bg-purple waves-effect">
                                                <i class="material-icons">search</i>
                                                <span>SEARCH</span>
                                                </button>

                                            </div>
                                        </div>
                                        </form>
                                    </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                <div class="panel-group" id="accordion_17" role="tablist" aria-multiselectable="true">

                                    @if($days2 !=null)
                                    @foreach($days2 as $day)
                                    <div class="panel panel-col-pink">
                                        <div class="panel-heading" role="tab" id="headingOne_17">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseOne_17" aria-expanded="true" aria-controls="collapseOne_17">
                                                    <i class="material-icons">perm_contact_calendar</i>

                                                    {{$day->date}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne_17" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_17">
                                            <div class="panel-body">

                                                <div class="table-responsive">

                                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                        <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Delivery DateTime</th>
                                                            <th>Product quantity</th>
                                                            <th>Total price</th>
                                                            <th>status_id</th>
                                                            <th>Created_at</th>
                                                            <th>Updated_at</th>

                                                        </tr>
                                                        </thead>
                                                        <tfoot>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Delivery DateTime</th>
                                                            <th>Product quantity</th>
                                                            <th>Total price</th>
                                                            <th>status_id</th>
                                                            <th>Created_at</th>
                                                            <th>Updated_at</th>
                                                        </tr>
                                                        <h4>Recette du jour: {{$day->total}} DT</h4>
                                                        </tfoot>
                                                        <tbody>
                                                        @foreach($orders as $order)
                                                            @if($day->date== Carbon\Carbon::parse($order->updated_at)->format('Y-m-d') )

                                                            <tr>
                                                                <td tabindex="1">{{$order->id}}</td>
                                                                <td tabindex="1">{{$order->delivery_date_time}}</td>
                                                                <td tabindex="1">{{$order->product_quantity}}</td>
                                                                <td tabindex="1">{{$order->total_price}}</td>
                                                                <td tabindex="1">{{$order->delivery_status_id}}</td>
                                                                <td tabindex="1">{{$order->created_at}}</td>
                                                                <td tabindex="1">{{$order->updated_at}}</td>
                                                            </tr>

                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                    @elseif($days2==null)

                                            @foreach($days as $day)
                                                <div class="panel panel-col-pink">
                                                    <div class="panel-heading" role="tab" id="headingOne_17">
                                                        <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseOne_17" aria-expanded="true" aria-controls="collapseOne_17">
                                                                <i class="material-icons">perm_contact_calendar</i>

                                                                {{$day->date}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne_17" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_17">
                                                        <div class="panel-body">

                                                            <div class="table-responsive">

                                                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Id</th>
                                                                        <th>Delivery DateTime</th>
                                                                        <th>Product quantity</th>
                                                                        <th>Total price</th>
                                                                        <th>status_id</th>
                                                                        <th>Created_at</th>
                                                                        <th>Updated_at</th>

                                                                    </tr>
                                                                    </thead>
                                                                    <tfoot>
                                                                    <tr>
                                                                        <th>Id</th>
                                                                        <th>Delivery DateTime</th>
                                                                        <th>Product quantity</th>
                                                                        <th>Total price</th>
                                                                        <th>status_id</th>
                                                                        <th>Created_at</th>
                                                                        <th>Updated_at</th>
                                                                    </tr>
                                                                    <h4>Recette du jour: {{$day->total}} DT</h4>
                                                                    </tfoot>
                                                                    <tbody>
                                                                    @foreach($orders as $order)
                                                                        @if($day->date== Carbon\Carbon::parse($order->updated_at)->format('Y-m-d') )

                                                                            <tr>
                                                                                <td tabindex="1">{{$order->id}}</td>
                                                                                <td tabindex="1">{{$order->delivery_date_time}}</td>
                                                                                <td tabindex="1">{{$order->product_quantity}}</td>
                                                                                <td tabindex="1">{{$order->total_price}}</td>
                                                                                <td tabindex="1">{{$order->delivery_status_id}}</td>
                                                                                <td tabindex="1">{{$order->created_at}}</td>
                                                                                <td tabindex="1">{{$order->updated_at}}</td>
                                                                            </tr>

                                                                        @endif
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        </div>
    </section>
@endsection
@section("footerJs")
    @parent

    <script src="{{ URL::asset("plugins/autosize/autosize.js")}}"></script>
    <script src="{{ URL::asset("plugins/momentjs/moment.js")}}"></script>
    <script src="{{ URL::asset("plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js")}}"></script>
    <script src="{{ URL::asset("plugins/bootstrap-datepicker/js/bootstrap-datepicker.js")}}"></script>
    <script src="{{ URL::asset("js/pages/forms/basic-form-elements.js")}}"></script>


@endsection
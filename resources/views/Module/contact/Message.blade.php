@extends('layouts/app')
@section('title','contact')
@section('topCss')
    @parent
    <link href="{{ URL::asset("plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css")}}"
          rel="stylesheet">
@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div><br/>
                            @endif
                            @foreach($messages as $m)
                                <div class="panel panel-default panel-post">

                                    <div class="panel-heading">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <img src="../../images/user-lg.jpg">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <a href="#">{{$m->nom}}</a>
                                                </h4>
                                                <small style="float: right">
                                                    {{$m->created_at}}
                                                </small>
                                                {{$m->email}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div style="float: right" class="col-md-1">
                                            <a href="{{ route('deleteMsg',$m->id)}}" type="button" class="close"
                                               aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </a>
                                        </div>
                                        <div class="post">
                                            <div class="post-heading">
                                                <h6><strong>Sujet: </strong>{{$m->sujet}}</h6>
                                                <p>{{$m->message}}</p>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <button type="button" class="btn btn-default waves-effect m-r-20"
                                                data-toggle="modal" data-target="#defaultModal">REPONDRE
                                        </button>
                                    </div>

                                </div>
                            @endforeach


                        </div>
                    </div>
                </div>

                <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog"
                     style="display: none;">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="defaultModalLabel">Email</h4>
                            </div>
                            <div class="modal-body">
                                <form action="{{route('send')}}" method="post">
                                    @csrf
                                    <label>à</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="email" class="form-control"
                                                   value="{{$m->email}}" placeholder="email">
                                        </div>
                                    </div>
                                    <label>Sujet</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="sujet" name="sujet"
                                                   class="form-control"
                                                   placeholder="sujet">
                                        </div>
                                    </div>
                                    <label for="password">Message</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="password" name="message"
                                                   class="form-control"
                                                   placeholder="Enter votre message">
                                        </div>
                                    </div>

                                    <br>
                                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">
                                        REPONDRE
                                    </button>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-link waves-effect"
                                        data-dismiss="modal">CLOSE
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("footerJs")
    @parent
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ URL::asset("plugins/jquery-datatable/jquery.dataTables.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/buttons.flash.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/jszip.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/pdfmake.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/vfs_fonts.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/buttons.html5.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/buttons.print.min.js")}}"></script>

    <script src="{{ URL::asset("js/pages/tables/jquery-datatable.js")}}"></script>
    <script>
        function autoRefreshPage() {
            window.location = window.location.href;
        }

        setInterval('autoRefreshPage()', 100000);
    </script>
@endsection
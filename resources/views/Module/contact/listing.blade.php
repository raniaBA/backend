@extends('layouts.app')
@section('title','contact')
@section('topCss')
    @parent

@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                CONTACT
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">

                            <ul class="list-group">

                                @foreach($emails as $em)

                                    <a href="http://test2.test/deleteCartItem/16" type="button" class="close"
                                       aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </a>


                                    <a href="{{action('ContactController@messages',$em->email)}}" type="submit"
                                       class="list-group-item">{{$em->email}}
                                        @if($em->total>0)
                                            <span class="badge bg-pink">{{$em->total}} new </span>
                                        @endif
                                    </a>


                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @section("footerJs")
                @parent


        </div>
    </section>
@endsection

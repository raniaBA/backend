@extends('layouts/app')
@section('title','user')
@section('topCss')
    @parent
     <link href="{{ URL::asset("plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css")}}" rel="stylesheet">

@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
             <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ORDERS TABLE
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @endif
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Delivery DateTime</th>
                                        <th>Product quantity</th>
                                        <th>Total price</th>
                                        <th>status_id</th>
                                        <th>Created_at</th>
                                        <th>Updated_at</th>
                                            <th>Option</th>
                                            <th>Option</th>

                                        <th>Option</th>

                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Delivery DateTime</th>
                                        <th>Product quantity</th>
                                        <th>Total price</th>
                                        <th>status_id</th>
                                        <th>Created_at</th>
                                        <th>Updated_at</th>
                                         <th>Option</th>
                                         <th>Check</th>
                                        <th>Option</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>

                                    @foreach($orders  as $order)
                                        <tr>
                                            <td tabindex="1">{{$order->id}}</td>
                                            <td tabindex="1">{{$order->delivery_date_time}}</td>
                                            <td tabindex="1">{{$order->product_quantity}}</td>
                                            <td tabindex="1">{{$order->total_price}}</td>
                                            <td tabindex="1">{{$order->delivery_status_id}}</td>
                                            <td tabindex="1">{{$order->created_at}}</td>
                                            <td tabindex="1">{{$order->updated_at}}</td>

                                            @if($order->delivery_status_id=='1')
                                                <td tabindex="1"><a href="{{action('OrdersController@edit',$order->id)}}" type="submit" class="btn btn-primary waves-effect">Modifier</a></td>
                                                <form  action="{{action('OrdersController@modif',$order->id)}}" method="post">
                                                    {{csrf_field()}}
                                                    <td tabindex="1"><button type="submit" class="btn btn-primary waves-effect">Livré</button></td>
                                                </form>


                                            <td tabindex="1">

                                                <form  action="{{action('OrdersController@destroy',$order->id)}}" method="post">
                                                    {{csrf_field()}}
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button type="submit" class="btn btn-primary waves-effect">Supprimer</button>
                                                </form>

                                            </td>
                                            @endif
                                            @if($order->delivery_status_id=='2')
                                                <td tabindex="1">
                                                    <button type="button" class="btn btn-success waves-effect">
                                                        <i style="top:0px" class="material-icons">check_circle</i>
                                                    </button>
                                                </td>
                                            @endif
                                            @if($order->delivery_status_id=='3')
                                                <td tabindex="1">
                                                    <a  href="{{action('OrdersController@edit',$order->id)}}" type="submit"  class="btn bg-red waves-effect">
                                                        <i class="material-icons">trending_up</i>
                                                        <span>en cours</span>
                                                    </a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
@section("footerJs")
    @parent
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ URL::asset("plugins/jquery-datatable/jquery.dataTables.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/buttons.flash.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/jszip.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/pdfmake.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/vfs_fonts.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/buttons.html5.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/buttons.print.min.js")}}"></script>

    <script src="{{ URL::asset("js/pages/tables/jquery-datatable.js")}}"></script>
@endsection
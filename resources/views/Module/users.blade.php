@extends('layouts/app')
@section('title','user')
@section('topCss')
    @parent
     <link href="{{ URL::asset("plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css")}}" rel="stylesheet">

@endsection

@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
             <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                PRODUCT TABLE
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        @if(session()->get('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div><br />
                        @endif
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>name</th>
                                        <th>email</th>
                                        <th>phone</th>
                                        <th>delete</th>
                                        <th>commande</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>name</th>
                                        <th>email</th>
                                        <th>phone</th>
                                        <th>delete</th>
                                        <th>commande</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>

                                    @foreach($users  as $u)

                                        <tr>
                                            <td tabindex="1">{{$u->id}}</td>
                                            <td tabindex="1">{{$u->name}}</td>
                                            <td tabindex="1">{{$u->email}}</td>
                                            <td tabindex="1">{{$u->phone}}</td>

                                            <td tabindex="1">
                                                <form  action="{{route('users.destroy',$u->id)}}" method="post">
                                                    {{csrf_field()}}
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button type="submit" class="btn btn-primary waves-effect">Supprimer</button>
                                                </form>
                                            </td>
                                            <td tabindex="1">
                                                {{--<a href="{{action('UsersController@find',$u->id)}}" type="submit" class="btn btn-primary waves-effect">commandes</a>--}}
                                                <a type="submit"  class="btn btn-dark" href="{{ route('find',$u->id)}}">En savoir plus</a>

                                                {{--<form method="post" action="{{ URL::asset('/find',$u->id) }}">--}}
                                                    {{--@csrf--}}

                                                    {{--<button type="submit" class="btn btn-primary waves-effect">show commande</button>--}}
                                                {{--</form>--}}
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
@section("footerJs")
    @parent
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ URL::asset("plugins/jquery-datatable/jquery.dataTables.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/buttons.flash.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/jszip.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/pdfmake.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/vfs_fonts.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/buttons.html5.min.js")}}"></script>
    <script src="{{ URL::asset("plugins/jquery-datatable/extensions/export/buttons.print.min.js")}}"></script>

    <script src="{{ URL::asset("js/pages/tables/jquery-datatable.js")}}"></script>
@endsection
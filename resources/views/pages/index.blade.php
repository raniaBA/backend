@extends('layouts/app')
@section('title','index')
@section('topCss')
@parent

@endsection


@section('topJs')
    @parent

@endsection
@section("header")
    @parent

@endsection
@section("menu")
    @parent

@endsection
@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
@endsection
@section("footerJs")
    @parent
@endsection
@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <div class="row">
                <div class="col-md-3">
                    <img src="https://ween.tn/media/cache/my_thumb_fb/uploads/image/38005/38003/avatar/avatar.jpg"
                         width="150" alt="La clèche">
                </div>
                <div class="col-md-3">
                    La calèche
                </div>
            </div>



        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent

@extends('layouts.app')
@section('title','index')
@section('topCss')
    @parent

@endsection

@section('topJs')
    @parent

@endsection
{{--@section("header")--}}
{{--@section('bodyStyle','bg')--}}

{{--@endsection--}}
{{--@section("menu")--}}


{{--@endsection--}}

@section('content')

    <div class="login-page">

        <div class="login-box">
            <div class="logo">
                <a href="javascript:void(0);"><b>Admin</b></a>
                <small>Admin -  La calèche</small>
            </div>
            <div class="card">
                <div class="body">
                    <form id="sign_in" method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="msg">Sign in to start your session</div>
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                            <div class="form-line">
                                {{--<input type="text" class="form-control" name="username" placeholder="Username" required autofocus>--}}
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="E-mail">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                            <div class="form-line">
                                {{--<input type="password" class="form-control" name="password" placeholder="Password" required>--}}
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                            <div class="form-line">
                                {{--<input type="password" class="form-control" name="password" placeholder="Password" required>--}}
                                <input id="password-confirm" type="password" class="form-control" placeholder="confirm password" name="password_confirmation" required autocomplete="new-password">


                            </div>
                        </div>


                        <div class="row">
                            {{--<div class="col-xs-8 p-t-5">--}}
                                {{--<input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">--}}
                                {{--<label for="rememberme">Remember Me</label>--}}

                                {{--<input class="filled-in chk-col-pink" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                                {{--<label class="form-check-label" for="remember">--}}
                                    {{--{{ __('Remember Me') }}--}}
                                {{--</label>--}}
                            {{--</div>--}}
                            <div class="col-xs-4">
                                <button class="btn bg-red waves-effect" type="submit"> {{ __('Reset Password') }}</button>
                            </div>
                        </div>
                        {{--<div class="row m-t-15 m-b--20">--}}
                            {{--<div class="col-xs-6">--}}
                                {{--<a href="{{url('/register')}}">Register Now!</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-6 align-right">--}}
                                {{--@if (Route::has('password.request'))--}}
                                    {{--<a  href="{{ route('password.request') }}">--}}
                                        {{--{{ __('Forgot Your Password?') }}--}}
                                    {{--</a>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </form>
                </div>
            </div>


        </div>
    </div>

@endsection
@section("footerJs")
    @parent
@endsection


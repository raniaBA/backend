@extends('layouts.app')
@section('title','index')
@section('topCss')
    @parent

@endsection

@section('topJs')
    @parent

@endsection
@section('bodyStyle','bg')
{{--@section("header")--}}
 @section('bodyStyle','bg')

{{--@endsection--}}
{{--@section("menu")--}}


{{--@endsection--}}

@section('content')

    <div class="login-page">

        <div class="login-box">
            <div class="logo">
                <a href="javascript:void(0);"><b>Admin</b></a>
                <small>Admin -  La calèche</small>
            </div>
            <div class="card">
                <div class="body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form id="sign_in" method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="msg">Sign in to start your session</div>
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                            <div class="form-line">
                                {{--<input type="text" class="form-control" name="username" placeholder="Username" required autofocus>--}}
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-mail">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-xs-4">
                                <button class="btn bg-red waves-effect" type="submit"> {{ __('Send Password Reset Link') }}</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>


        </div>
    </div>

@endsection
@section("footerJs")
    @parent
@endsection


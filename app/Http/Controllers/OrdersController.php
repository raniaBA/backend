<?php

namespace App\Http\Controllers;

use App\delivery_status;
use App\orders;
use App\payment_method;
use App\produit;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    /**
     * OrdersController constructor.
     */

    public function __construct()
    {
         parent::__construct();
        $this->activeModule="";
        $this->activeAction="";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function create()
    {
        $produit=produit::All();

        $this->activeModule="order";
        $this->activeAction="addOrder";
        return view('Module.Orders.add',['activeModule'=>$this->activeModule,'produit'=>$produit,'activeAction'=>$this->activeAction]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'delivery_date_time'=>'required',

        ]);
        $orders = new orders([
            'delivery_date_time' => $request->get('delivery_date_time'),
            'client_id'=> 0,
         ]);
        $nbr = $request->get('nbr');
        $total=0;
        for ($i=1;$i<=$nbr; $i++){
            $string=strval($i);
            $pname=$request->get('produit_name'.$string);
            $qnt=$request->get('product_quantity'.$string);
            $product = DB::table('produits')->where('name', $pname)->first();

            $total=$total+($product->price*$qnt);

        }
        $orders->total_price=$total;


        $paiement = DB::table('payment_methods')->where('name', 'cache')->first();
        $orders->delivery_status_id=2;
        $orders->payment_id=$paiement->id;
        $orders->save();
        $latest=$order_id=DB::table('orders')->latest('id')->first();
        if($nbr==null){
            $pname=$request->get('produit_id1');
            $product = DB::table('produits')->where('name', $pname)->first();
            DB::table('orders_produit')->insert(
                ['produit_id' =>$product->id ,'quantity'=>$request->get('product_quantity1'),'orders_id' =>$latest->id,'created_at'=>now(),'updated_at'=>now()]
            );
        }
        for ($i=1;$i<=$nbr; $i++){
            $string=strval($i);
            $pname=$request->get('produit_id'.$string);
            $product = DB::table('produits')->where('name', $pname)->first();
            DB::table('orders_produit')->insert(
                ['product_id' =>$product->id ,'quantity'=>$request->get('product_quantity'.$string),'order_id' =>$latest->id,'created_at'=>now(),'updated_at'=>now()]
            );

        }


        return redirect('/order')->with('success','order has been added');
    }

    public function show()
    {
        $this->activeModule="order";
        $this->activeAction="attente";
        $val="attente";
        $orders = DB::table('orders')->where('delivery_status_id', '=','1')->get();

        return view('Module.Orders.listing',['orders'=>$orders,'val'=>$val,'activeModule'=>$this->activeModule,'activeAction'=>$this->activeAction]);
    }
    public function showLivre()
    {
        $this->activeModule="order";
        $this->activeAction="showLivree";
        $val="livre";
//        $orders=orders::where('delivery_status_id', '=', 2);
        $orders = DB::table('orders')->where('delivery_status_id', '=','2')->get();
//        $orders = App\orders::where('id', '=', 1)->firstOrFail();


        return view('Module.Orders.listing',['orders'=>$orders,'val'=>$val,'activeModule'=>$this->activeModule,'activeAction'=>$this->activeAction]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

      $orders = orders::find($id);
      $p= produit::all();

      $pivot = DB::table('orders_produit')->where('orders_id', '=',$id)->get();

      $produit=$orders->products;
        return view('Module.orders.edit', ['orders'=>$orders,'produit'=>$produit,'pivot'=>$pivot,'p'=>$p]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'delivery_date_time'=>'required',
        ]);

        $ord = orders::find($id);

        $ord->delivery_date_time = $request->get('delivery_date_time');
        $ord->delivery_status_id = $request->get('status');
        $status = delivery_status::where('id',$ord->delivery_status_id)->first();
        $client= DB::table('clients')->where('id', $ord->client_id)->get();

        $produits=$ord->products;
        $total=0;
        for ($i=0;$i<count($produits); $i++){

            $string=strval($i);
            $pname=$request->get('produit_name'.$string);
            $pqnt=$request->get('product_quantity'.$string);
            $prod = produit::where('name',$pname)->first();
            $total=$total+($produits[$i]->price*$pqnt);

            $pivot = DB::table('orders_produit')->where('produit_id', $produits[$i]->id)->where('orders_id', $ord->id)->delete();
            DB::table('orders_produit')->insert(
                ['produit_id' =>$prod->id ,'quantity'=>$pqnt,'orders_id' =>$ord->id,'created_at'=>now(),'updated_at'=>now()]
            );

        }

        $data = [
            'name'=>$client[0]->name,
            'message'=>'Votre commande est '.$status->name,
            'from'=>'caleche.test@gmail.com',
            'view'=>'to_client.message',
            'subject'=>'commade calche ',
        ] ;

        $this->sendMail($client[0]->email,$data);

        $ord->total_price=$total;

        $ord->save();


        return redirect('/order')->with('success', 'order has been updated');
    }
    /**
     * Update the specified resource in storage.
     *
     *
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */

    public function modif($id)
    {

        $ord = orders::find($id);

        $client= DB::table('clients')->where('id', $ord->client_id)->get();

        $status = DB::table('delivery_statuses')->where('name', '=','livre')->get();

        $ord->delivery_status_id =$status[0]->id;

        $data = [
            'name'=>$client[0]->name,
            'message'=>'Votre commande est livré',
            'from'=>'caleche.test@gmail.com',
            'view'=>'to_client.message',
            'subject'=>'commade calche ',
        ] ;

        $this->sendMail($client[0]->email,$data);

        $ord->save();

            return redirect('/order')->with('success', 'order has been updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $order = orders::find($id);
        DB::table('orders_produit')->where('orders_id', $id)->delete();
        $order->delete();
        if( $order->delivery_status_id ==1){
            return redirect('/order')->with('success', 'order has been updated');
        }
        else if($order->delivery_status_id ==2){
            return redirect('/showlivre')->with('success', 'order has been updated');
        }
     }

}

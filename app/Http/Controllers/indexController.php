<?php
/**
 * Created by PhpStorm.
 * User: spot-info
 * Date: 28/06/19
 * Time: 10:54
 */

namespace App\Http\Controllers;


use App\Contact;
use Illuminate\Support\Facades\DB;
class indexController extends Controller
{

    /**
     * indexController constructor.
     */
    /**
     * @return void
     */


    public function __construct()
    {

        parent::__construct();
         $this->activeModule="";

    }

     public function showIndex()
    {
        return view('pages.index');
    }
    public function contact(){
        $emails= DB::table('contacts')
            ->select('email', DB::raw('sum(vu) as total'))
            ->groupBy('email','vu')
            ->orderBy('vu','desc')
            ->get();
        $contact=new Contact();
        $this->activeModule="message";
        $colors = ['#F3F3F1', '#DFE9F4', '#E5E5E2', '#F4F4E3', '#F4E6DC', '#EEFAFC'];
        return view('Module.contact.listing',['emails'=>$emails,'activeModule'=>$this->activeModule]);
    }

}
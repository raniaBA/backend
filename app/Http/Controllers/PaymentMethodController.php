<?php

namespace App\Http\Controllers;

use App\payment_method;
use App\produit;
use Illuminate\Http\Request;

class PaymentMethodController extends Controller
{
    /**
     * PaymentMethodController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->activeModule="paymentMethod";
        $this->activeAction="paymentAdd";
        return view('Module.payment.add',['activeModule'=>$this->activeModule,'activeAction'=>$this->activeAction]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
        ]);
        $payment_method = new payment_method([
            'name' => $request->get('name'),
        ]);

        $payment_method->save();
        return redirect('/payment/show')->with('success','Method has been added');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $this->activeModule="paymentMethod";
        $this->activeAction="paymentShow";


        $payment_method=payment_method::All();

        return view('Module.payment.listing',['payment_method'=>$payment_method,'activeModule'=>$this->activeModule,'activeAction'=>$this->activeAction]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\payment_method  $payment_method
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment = payment_method::find($id);

        return view('Module.payment.edit', ['payment'=>$payment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\payment_method  $payment_method
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
        ]);

        $payment = payment_method::find($id);
        $payment->name = $request->get('name');


        $payment->save();

        return redirect('/payment/show')->with('success', 'payment has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\payment_method  $payment_method
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment = payment_method::find($id);

        if(isset($payment)){
            $payment->delete();
        }
        return redirect('/payment/show')->with('deletesuccess','method has been deleted!');
    }
}

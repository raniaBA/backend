<?php
/**
 * Created by PhpStorm.
 * User: spot-info
 * Date: 03/07/19
 * Time: 15:04
 */

namespace App\Http\Controllers;

use App\categorie;
use App\zone;
use Illuminate\Http\Request;

class ZoneController extends Controller
{
    /**
     * ZoneController constructor.
     */
    public function __construct()
    {
         parent::__construct();
        $this->activeModule="";
    }

    function showClient()
    {
        $this->activeModule="clientMap";
        $polygones = zone::All();

        if(sizeof($polygones)!=0) {
            $poly = explode(',', $polygones[0]->polygone);
            $i = 0;
            foreach ($poly as $p) {
                $pos = array("(", ")");
                $new = str_replace($pos, "", $p);
                $poly[$i] = $new;
                $i++;
            }
            return view('Module.Map.clientMap',['activeModule'=>$this->activeModule,'poly'=>$poly]);
        }else{

            return view('Module.Map.adminMap');
        }

    }

    function show()
    {
        $this->activeModule="adminMap";
        return view('Module.Map.adminMap',['activeModule'=>$this->activeModule]);
    }

    public function store(Request $request){
        $polygones = zone::All();
            $zone = new zone([
            'polygone' => $request->get('polygone'),
        ]);
        if(sizeof($polygones)==0){
            $zone->save();
        }else{
        $z = zone::find(1);
        $z->polygone = $request->get('polygone');
        $z->save();
        }
        return redirect('/adminMap')->with('success', 'zone has been saved');
    }

}
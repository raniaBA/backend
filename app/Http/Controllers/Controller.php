<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use App\Mail\Mailer ;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $activeModule;
    public $activeAction;
    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->activeModule="";
        $this->activeAction="";
        view::share('activeModule', $this->activeModule);
        view::share('activeAction', $this->activeAction);
    }

    protected function sendMail($to ,  array $data = []) {

        Mail::to($to)
            ->send(new Mailer($data)) ;

    }

}

<?php

namespace App\Http\Controllers;

use App\delivery_status;
use Illuminate\Http\Request;

class DeliveryStatusController extends Controller
{
    /**
     * DeliveryStatusController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\delivery_status  $delivery_status
     * @return \Illuminate\Http\Response
     */
    public function show(delivery_status $delivery_status)
    {

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\delivery_status  $delivery_status
     * @return \Illuminate\Http\Response
     */
    public function edit(delivery_status $delivery_status)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\delivery_status  $delivery_status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, delivery_status $delivery_status)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\delivery_status  $delivery_status
     * @return \Illuminate\Http\Response
     */
    public function destroy(delivery_status $delivery_status)
    {
        //
    }
}

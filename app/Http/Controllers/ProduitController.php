<?php
/**
 * Created by PhpStorm.
 * User: spot-info
 * Date: 28/06/19
 * Time: 14:53
 */

namespace App\Http\Controllers;



use App\categorie;
use App\produit;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;
class ProduitController extends Controller
{
    public function __construct()
    {
         parent::__construct();
        $this->activeModule="";
    }
    public function showProduit()
    {

        $this->activeModule="produit";
        $this->activeAction="show";
        $produits=produit::all();
        return view('Module.Produit.listing',['produits'=>$produits,'activeModule'=>$this->activeModule,'activeAction'=>$this->activeAction]);


    }

    /*public function editProduit(){
        return view('Module.Produit.edit');
    }*/
    public function addProduit(){
        $categories=categorie::all();
        $this->activeModule="produit";

        $this->activeAction="add";
        return view('Module.Produit.add',['activeAction'=>$this->activeAction,'activeModule'=>$this->activeModule,'categories'=>$categories]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public  function store(Request $request)
    {
        $produit = new produit();
        $data = $request->validate([
            'name'=>'required',
            'description'=>'required',
            'image'=>'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'price'=>'required',
            'categorie_id'=>'required'
        ]);

        // Get image file
        $image = $request->file('image');

        // Make a image name based on user name and current timestamp
        $name = str_slug($request->input('name')).'_'.time();
        // Define folder path
        $folder = '/uploads/imagesPro/';
        // Make a file path where image will be stored [ folder path + file name + file extension]
        // Upload image
        $name = str_random(25);

        $filePath = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
        $image = Image::make($filePath);

        $image->resize(150, 150);
        $image->save($filePath);
        // Set user profile image path in database to filePath

        // Set user profile image path in database to filePath
        $path = $filePath;

        $produit->saveProduit($data,$path);
        return redirect('/listProd')->with('addsuccess','product has been stored!');
    }


    public function edit($id)

    {
        $produit = produit::where('id', $id)
            ->first();
        $categories=categorie::all();

        return view('Module.Produit.edit',['categories'=>$categories],compact('produit','id'));

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produit = produit::where('id', $id)
            ->first();

       // dd($produit->image);

            $data = $request->validate([
                'name' => 'required',
                'description' => 'required',
                'price' => 'required',
                'categorie_id' => 'required'

            ]);
        if ($produit->image!="") {


                // Get image file
                $image = $request->file('image');

                // Define folder path
                $folder = '/uploads/imagesPro/';
                // Make a file path where image will be stored [ folder path + file name + file extension]
                // Upload image
                $name = str_random(25);
            if($request->image)
            {
                $filePath = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
                $image = Image::make($filePath);

                $image->resize(150, 150);
                $image->save($filePath);

                // Set user profile image path in database to filePath
                $path = $filePath;
                if(isset($produit->image)){
                    File::delete($produit->image);
                }

            } else{
                $data['image'] = $produit->image;
                $path=$produit->image;
            }

        } else
            {

            $data = $request->validate([
                'image' => 'required',
            ]);
            // Get image file
            $image = $request->file('image');

            // Define folder path
            $folder = '/uploads/imagesPro/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            // Upload image
            $name = str_random(25);

            $filePath = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
                $image = Image::make($filePath);

                $image->resize(150, 150);
                $image->save($filePath);

            // Set user profile image path in database to filePath
            $path = $filePath;
    }




        $data['id'] = $id;

        $produit->updateProduit($data,$path);


        return redirect('/listProd')->with('editsuccess','product has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $produit = produit::find($id);
        $produit->delete();
        if(isset($produit->image)){
            File::delete($produit->image);
        }
        return redirect('/listProd')->with('deletesuccess','product has been deleted!');
    }
}
<?php

namespace App\Http\Controllers;

use App\orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * UsersController constructor.
     */
    public function __construct()
    {
         parent::__construct();
        $this->activeModule="";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function show()
    {
        $this->activeModule="users";
        $users= DB::table('clients')->get();
        return view('Module.users',['activeModule'=>$this->activeModule,'users'=>$users]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = DB::table('clients')->where('id','=',$id);
        $user->delete();

        return redirect('/users/show')->with('success', 'user has been deleted Successfully');
    }
    /**
     * find the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function find($id)
    {
        $this->activeModule="order";
        $this->activeAction="showLivree";
        $val="attente";
         $orders = Orders::where('client_id', '=', $id)->get();

            return view('Module.userOrders',['orders'=>$orders,'activeModule'=>$this->activeModule,'activeAction'=>$this->activeAction]);



    }
}

<?php
/**
 * Created by PhpStorm.
 * User: spot-info
 * Date: 28/06/19
 * Time: 12:49
 */

namespace App\Http\Controllers;



use App\categorie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Traits\UploadTrait;
use Intervention\Image\ImageManagerStatic as Image;

class CategorieController extends Controller
{

    /**
     * CategorieController constructor.
     */
    public function __construct()
    {

        parent::__construct();
        $this->activeModule="";
    }

    public function showCategorie()
    {
        $this->activeModule="categorie";
        $this->activeAction="showc";


        $categories=Categorie::All();

        return view('Module.Categorie.listing',['categories'=>$categories,'activeModule'=>$this->activeModule,'activeAction'=>$this->activeAction]);
    }

//    public function editCategorie(){
//        return view('Module.Categorie.edit');
//    }
//    public function addCategorie(){
//        return view('Module.Categorie.add');
//    }
    public function create()
    {
        $this->activeModule="categorie";

        $this->activeAction="addc";
        $activeAction=$this->activeAction;
        return view('Module.Categorie.add',['activeAction'=>$this->activeAction,'activeModule'=>$this->activeModule]);
    }
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'icon'=> 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'parent' => 'required|integer'
        ]);

        $categorie = new Categorie([
            'name' => $request->get('name'),
            'parent'=> $request->get('parent')
        ]);
        // Get image file
        $image = $request->file('icon');

        // Make a image name based on user name and current timestamp
        // Define folder path

        $folder = '/uploads/images/';

        // Make a file path where image will be stored [ folder path + file name + file extension]
        // Upload image
        $name = str_random(25);


        $filePath = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
        $image = Image::make($filePath);

        $image->resize(150, 150);
        $image->save($filePath);

        $categorie->icon=$filePath;
        // Set user profile image path in database to filePath


        $categorie->save();
        return redirect('/categ')->with('success', 'categorie has been added');
    }

    public function destroy($id)
    {
        $cat = Categorie::find($id);
        $cat->delete();
        if(isset($cat->icon)){
            File::delete($cat->icon);
        }

        return redirect('/categ')->with('success', 'Categorie has been deleted Successfully');
    }
    public function edit($id)
    {
        $this->activeAction="edit";
        $cat = Categorie::find($id);

        return view('Module.Categorie.edit', compact('cat'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'icon',
            'parent' => 'required|integer'
        ]);

        $cat = Categorie::find($id);
        $cat->name = $request->get('name');
        $cat->parent = $request->get('parent');

        if ($cat->icon!="") {

            // Get image file
            $image = $request->file('icon');

            // Make a image name based on user name and current timestamp
            $name = str_slug($request->input('name')).'_'.time();
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            // Upload image
            $name = str_random(25);
            if($request->icon)
            {
                if(isset($cat->icon)){
                    File::delete($cat->icon);
                }
                $filePath = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
                $image = Image::make($filePath);

                $image->resize(150, 150);
                $image->save($filePath);
                $cat->icon = $filePath;


            }else{
                $cat->icon ;

            }

        }else{
            $request->validate([
                'icon' => 'required',
            ]);
            // Get image file
            $image = $request->file('icon');

            // Make a image name based on user name and current timestamp
            $name = str_slug($request->input('name')).'_'.time();
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            // Upload image
            $name = str_random(25);
            $filePath = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');

            $cat->icon = $filePath;

            $image = Image::make($filePath);

            $image->resize(150, 150);
            $image->save($filePath);
        }


        // Set user profile image path in database to filePath
        $cat->save();

        return redirect('/categ')->with('success', 'categorie has been updated');
    }
}
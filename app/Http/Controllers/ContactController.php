<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    /**
     * ContactController constructor.
     */
    public function __construct()
    {

        parent::__construct();
        $this->activeModule="";
    }

    public function messages($email){
         $messages=Contact::orderBy('created_at', 'DESC')->where('email',$email)->get();


        foreach ($messages as $m){
            $m->vu=0;
            $m->save();
        }

        $this->activeModule="message";
        if(count($messages)!==0){

        return view('Module.contact.Message',['messages'=>$messages,'activeModule'=>$this->activeModule]);
        }else{

            return redirect('/contact');

        }
    }
    public function send(Request $request){

        $data = [
            'name'=>'caléche',
            'message'=>$request->get('message'),
            'from'=>'caleche.test@gmail.com',
            'view'=>'to_client.emailResp',
            'subject'=>$request->get('sujet'),
        ] ;

        $this->sendMail($request->get('email'),$data);
        return redirect()->back()->withErrors('message envoyé à '.$request->get('email'));
    }
    public function delete($id)
    {

        $contact = Contact::find($id);

        $contact->delete();
        return  redirect()->back();
    }
}

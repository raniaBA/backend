<?php
/**
 * Created by PhpStorm.
 * User: spot-info
 * Date: 28/06/19
 * Time: 10:54
 */

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class profileController extends Controller
{


    /**
     * profileController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showProfile()

    {
        return view('Module.profile');
    }
    public function profileSetting(Request $request,$id)

    {
        $request->validate([
            'name'=>'required',
            'email' => 'required'
        ]);

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->save();
        return redirect('/profile')->with('success','profile updated with success');
    }
    public function changePassword(Request $request,$id)

    {
        $request->validate([
            'old'=>'required',
            'new' => 'required',
            'new2' => 'required'
        ]);


        $user = User::find($id);
        $old=$request->get('old');
        $new=$request->get('new');
        $new2=$request->get('new2');

        if( Hash::check($old, $user->password)){
            if($new == $new2){
                $user->password = Hash::make($new);
                $user->save();
            }else{
                return redirect('profile')->withErrors('verifier votre nouveau mot de passe');
            }


        return redirect('/profile')->with('success','profile updated with success');
        }else{
            return redirect('/profile')->withErrors('verifier votre ancien mot de passe');

        }


        return view('Module.profile')->with('success','profile updated with success');
    }

}
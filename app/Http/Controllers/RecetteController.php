<?php

namespace App\Http\Controllers;

use App\orders;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RecetteController extends Controller
{

    /**
     * RecetteController constructor.
     */
    public function __construct()
    {
         parent::__construct();
        $this->activeModule="";
        $this->activeAction="";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function store(Request $request)
    {


    }
    public function filter(Request $request)
    {
        $debut = $request->get('debut');
        $fin = $request->get('fin');
        $fin2 = date('Y-m-d', strtotime($fin));
        $debut2 = date('Y-m-d', strtotime($debut));
        $orders = orders::All();

//        $days2 = DB::table('orders')
//            ->select('updated_at as date')
//            ->whereBetween('updated_at', [$debut2, $fin2])
//            ->orderBy('updated_at', 'desc')
//            ->get();
        $days2 = DB::table('orders')
            ->select(DB::raw('sum(total_price) as total ,DATE(updated_at) as date '))
            ->whereBetween('updated_at', [$debut2, $fin2])
            ->groupBy('date')
            ->orderBy('date', 'desc')
            ->get();
        return view('Module.recette', ['orders' => $orders, 'days2' => $days2]);



    }


    public function show(Request $request)
    {
        $this->activeModule="recette";
        $orders = orders::All();
        $days = DB::table('orders')
            ->select(DB::raw('sum(total_price) as total ,DATE(updated_at) as date '))
            ->where( 'updated_at', '>', Carbon::now()->subDays(7))
            ->groupBy('date')
            ->orderBy('date', 'desc')
            ->get();

        $days2=null;
        return view('Module.recette', ['orders' => $orders, 'days' => $days, 'days2' => $days2,'activeModule'=>$this->activeModule]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\recette $recette
     * @return \Illuminate\Http\Response
     */
    public function edit(recette $recette)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\recette $recette
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, recette $recette)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\recette $recette
     * @return \Illuminate\Http\Response
     */
    public function destroy(recette $recette)
    {
        //
    }
}

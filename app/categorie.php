<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categorie extends Model
{
    protected $fillable = [
        'parent' ,'icon', 'name',
    ];


    public function produits()
    {
        return $this->hasMany('App\produit');
    }
//    public function getImageAttribute()
//    {
//        return $this->icon;
//    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orders extends Model
{
    protected $date = ['updated_at'];
    protected $fillable =['delivery_date_time','total_price','delivery_status_id','client_id','recette_id'];

    public function satatus()
    {
        return $this->belongsTo('App\delivery_status', 'delivery_status_id');
    }
    public function recette()
    {
        return $this->belongsTo('App\recette', 'recette');
    }
    public function products(){
        return $this->belongsToMany(produit::class);

    }
    public function client()
    {
        return $this->belongsTo('App\User', 'client_id');
    }
    public function payment_method()
    {
        return $this->belongsTo('App\payment_method', 'payment_id');
    }
}

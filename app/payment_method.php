<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payment_method extends Model
{
    protected $fillable =['name'];

    public function orders()
    {
        return $this->hasMany('App\orders');
    }

}

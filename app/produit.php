<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class produit extends Model
{
    protected $fillable =['name','description','image','price','categorie_id'];
    public function saveProduit($data,$path){

        $this->name = $data['name'];
        $this->description = $data['description'];
        $this->image = $path;
        $this->price = $data['price'];
        $categorie = $data['categorie_id'];
//        dd($categorie);
        $categ = categorie::where('name', '=', $categorie)->firstOrFail();

        $this->categorie_id=$categ->id;

        $this->save();
        return 1 ;
    }

    public function updateProduit($data,$path)
    {
        $produit = $this->find($data['id']);
        $produit->name = $data['name'];
        $produit->description = $data['description'];
        $produit->image = $path;
        $produit->price = $data['price'];
        $categorie = $data['categorie_id'];
//        dd($categorie);
        $categ = categorie::where('name', '=', $categorie)->firstOrFail();

        $produit->categorie_id=$categ->id;

        $produit->save();
        return 1;
    }
    public function categorie()
    {
        return $this->belongsTo('App\categorie', 'categorie_id');
    }
    public function orders(){
        return $this->belongsToMany(orders::class);

    }
//    public function getImageAttribute()
//    {
//        return $this->image;
//    }

}

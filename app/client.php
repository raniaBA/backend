<?php
/**
 * Created by PhpStorm.
 * User: spot-info
 * Date: 18/07/19
 * Time: 13:34
 */

namespace App;
use Illuminate\Notifications\Notifiable;

class client
{
    use Notifiable;

    protected $table = 'clients';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone','gouvernorat','prenom',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function orders()
    {
        return $this->hasMany('App\orders');
    }

}
